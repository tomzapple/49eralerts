//
//  EventsDetailViewController.m
//  49erAlerts
//
//  Created by lsta on 11/14/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import "EventsDetailViewController.h"

@interface EventsDetailViewController ()

@end

@implementation EventsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_eventTitle setText:[_event objectForKey:@"title"]];
    [_eventDetailsDescription setText:[_event objectForKey:@"description"]];
    
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:[_event objectForKey:@"link"]];
    [attributedString addAttribute: NSLinkAttributeName value: [_event objectForKey:@"link"] range: NSMakeRange(0, attributedString.length)];
    

    [_eventLink setAttributedTitle:attributedString forState:UIControlStateNormal];
    [_eventLink addTarget:self
               action:@selector(onEventLinkClicked:)
     forControlEvents:UIControlEventTouchDown];
    
    UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[_event objectForKey:@"image"]]]];
    [_eventImage setImage:image];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) onEventLinkClicked:(id)sender{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_eventLink titleLabel].text]];
    
}
@end
