//
//  BrochureTableViewController.m
//  49erAlerts
//
//  Created by lsta on 3/5/15.
//  Copyright (c) 2015 UNCCLibrary. All rights reserved.
//

#import "BrochureTableViewController.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "SimplePDFWebViewController.h"

@interface BrochureTableViewController ()
@property (nonatomic, strong) NSArray *brochureArray;
@property (nonatomic, strong) NSMutableArray *brochureLinksArray;

@end

@implementation BrochureTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Brochures"];
    //    [query getObjectInBackgroundWithId:@"xWMyZ4YEGZ" block:^(PFObject *gameScore, NSError *error) {
    //        // Do something with the returned PFObject in the gameScore variable.
    //        NSLog(@"%@", gameScore);
    //    }];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        _brochureArray = objects;
        _brochureLinksArray = [NSMutableArray arrayWithCapacity:5];
        
        for(PFObject *object in _brochureArray){
            [PFConfig getConfigInBackgroundWithBlock:^(PFConfig *config, NSError *error) {
                NSString *fileName = [object objectForKey:@"fileName"];
                PFFile *pdfFile = config[fileName];
                NSURL *pdfURL = [NSURL URLWithString:pdfFile.url];
                [_brochureLinksArray addObject:pdfURL];
            }];
        }
        
        
        [self.tableView reloadData];
    }];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goToMainScreen:(id)sender{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    [appDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:^{
        ;
    }];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [_brochureArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell1";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    // Configure the cell with the textContent of the Post as the cell's text label
    //    PFObject *post = [_eventArray objectAtIndex:indexPath.row];
    NSDictionary *brochureNode = [_brochureArray objectAtIndex:indexPath.row] ;
    NSLog(@"Brochure File %@", [brochureNode objectForKey:@"fileName"]);
    [cell.textLabel setText:[brochureNode objectForKey:@"fileName"]];
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"ShowBrochureDetails"]) {
        SimplePDFWebViewController *detailViewController = [segue destinationViewController];
        
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        detailViewController.pdfURL = _brochureLinksArray[indexPath.row] ;
        
        
    }
    
    
}


@end
