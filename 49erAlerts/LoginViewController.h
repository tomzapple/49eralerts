//
//  LoginViewController.h
//  49erAlerts
//
//  Created by lsta on 10/15/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import "ViewController.h"

@interface LoginViewController : ViewController
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)loginButtonClicked:(id)sender;


@end
