//
//  SimplePDFWebViewController.m
//  49erAlerts
//
//  Created by lsta on 3/5/15.
//  Copyright (c) 2015 UNCCLibrary. All rights reserved.
//

#import "SimplePDFWebViewController.h"
#import <Parse/Parse.h>
#import "AppDelegate.h"

@interface SimplePDFWebViewController ()
@property (nonatomic,strong) UIWebView* webView;
@property (nonatomic,strong) UINavigationBar* navBar;
@end

@implementation SimplePDFWebViewController

- (void)viewDidLoad {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    CGRect rect = [[UIScreen mainScreen] bounds];
    CGSize screenSize = rect.size;
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,50,screenSize.width,screenSize.height)];
    
    _navBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(0, 0, screenSize.width, 50)];
    UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@""] ;
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(aMethod:)] ;
    navItem.rightBarButtonItem = done;
    _navBar.items = [NSArray arrayWithObject:navItem];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:_pdfURL];
    _webView.scalesPageToFit = YES;
    [_webView loadRequest:request];
    [self.view addSubview:_navBar];
    [self.view addSubview:_webView];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (BOOL)shouldAutorotate{
//    return NO;
//}

- (void) didRotate:(NSNotification *)notification
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    if ((orientation == UIDeviceOrientationLandscapeLeft) || (orientation == UIDeviceOrientationLandscapeRight) || (orientation == UIDeviceOrientationPortrait) || (orientation == UIDeviceOrientationPortraitUpsideDown))
    {
        NSLog(@"Landscape !");
        [_webView removeFromSuperview];
        [_navBar removeFromSuperview];
        CGRect rect = [[UIScreen mainScreen] bounds];
        CGSize screenSize = rect.size;
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,50,screenSize.width,screenSize.height)];
        
        _navBar = [[UINavigationBar alloc]initWithFrame:CGRectMake(0, 0, screenSize.width, 50)];
        UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@""] ;

        _navBar.items = [NSArray arrayWithObject:navItem];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:_pdfURL];
        _webView.scalesPageToFit = YES;
        [_webView loadRequest:request];
        [self.view addSubview:_navBar];
        [self.view addSubview:_webView];
        
    }
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
