//
//  ViewController.h
//  49erAlerts
//
//  Created by lsta on 10/2/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIPageViewControllerDataSource>

@property (weak, nonatomic) IBOutlet UIButton *startWalkThrough;

@property (strong, nonatomic) UIPageViewController *onboardingPageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;

@property (strong, nonatomic) UINavigationController *loginNavigationController;

@property (strong, nonatomic) UIViewController *pdfRootViewController;

- (IBAction)startWalkThrough:(id)sender;
- (IBAction)startUsingApplication:(id)sender;
- (IBAction)showTheMenuOptions:(id)sender;

@end

