//
//  OnboardingPageContentViewController.m
//  49erAlerts
//
//  Created by lsta on 10/6/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import "OnboardingPageContentViewController.h"

@interface OnboardingPageContentViewController ()

@end

@implementation OnboardingPageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.backgroundImageView.image = [UIImage imageNamed:self.imageFile];
    self.titleLabel.text = self.titleText;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
