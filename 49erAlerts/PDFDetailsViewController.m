//
//  DetailsViewController.m
//  49erAlerts
//
//  Created by lsta on 10/17/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import "PDFDetailsViewController.h"
#import "PDFScrollView.h"
#import "TiledPDFView.h"
#import "AppDelegate.h"

@interface PDFDetailsViewController ()

@property (strong) UIButton* goToWelcomeScreenButton;

@end

@implementation PDFDetailsViewController

-(void) dealloc {
    if( self.page != NULL ) CGPDFPageRelease( self.page );
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    if (_goToWelcomeScreenButton == nil){
        _goToWelcomeScreenButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_goToWelcomeScreenButton addTarget:self
                               action:@selector(aMethod:)
                     forControlEvents:UIControlEventTouchUpInside];
        [_goToWelcomeScreenButton setTitle:@"Go to Main Screen" forState:UIControlStateNormal];
        _goToWelcomeScreenButton.frame = CGRectMake(self.scrollView.center.x, 20, 160.0, 40.0);
        [_goToWelcomeScreenButton.layer setCornerRadius:8.0f];
        [_goToWelcomeScreenButton.layer setMasksToBounds:YES];
        [_goToWelcomeScreenButton.layer setBorderWidth:1.0f];
        [_goToWelcomeScreenButton.layer setBorderColor:[[UIColor whiteColor] CGColor]];
        _goToWelcomeScreenButton.backgroundColor = [UIColor greenColor];
        [self.view addSubview:_goToWelcomeScreenButton];
    }
    
    

    
    self.page = CGPDFDocumentGetPage( self.pdf, self.pageNumber );
    NSLog(@"self.page==NULL? %@",self.page==NULL?@"yes":@"no");
    
    if( self.page != NULL ) CGPDFPageRetain( self.page );
    [self.scrollView setPDFPage:self.page];
}

-(void)viewWillAppear:(BOOL)animated {
    // Disable zooming if our pages are currently shown in landscape
    if( self.interfaceOrientation == UIInterfaceOrientationPortrait || self.interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown ) {
        [self.scrollView setUserInteractionEnabled:YES];
    } else {
        [self.scrollView setUserInteractionEnabled:NO];
    }
    NSLog(@"%s scrollView.zoomScale=%f",__PRETTY_FUNCTION__,self.scrollView.zoomScale);
    
}

-(void)viewDidLayoutSubviews {
    NSLog(@"%s",__PRETTY_FUNCTION__);
    [self restoreScale];
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    NSLog(@"%s",__PRETTY_FUNCTION__);
    if( fromInterfaceOrientation == UIInterfaceOrientationPortrait || fromInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown ) {
        [self.scrollView setUserInteractionEnabled:NO];
    } else {
        [self.scrollView setUserInteractionEnabled:YES];
    }
}

-(void)aMethod:(id)sender{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    [appDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:^{
        ;
    }];
    
}

-(void)restoreScale {
    // Called on orientation change.
    // We need to zoom out and basically reset the scrollview to look right in two-page spline view.
    CGRect pageRect = CGPDFPageGetBoxRect( self.page, kCGPDFMediaBox );
    CGFloat yScale = self.view.frame.size.height/pageRect.size.height;
    CGFloat xScale = self.view.frame.size.width/pageRect.size.width;
    self.myScale = MIN( xScale, yScale );
    NSLog(@"%s self.myScale=%f",__PRETTY_FUNCTION__, self.myScale);
    self.scrollView.bounds = self.view.bounds;
    self.scrollView.zoomScale = 1.0;
    self.scrollView.PDFScale = self.myScale;
    self.scrollView.tiledPDFView.bounds = self.view.bounds;
    self.scrollView.tiledPDFView.myScale = self.myScale;
    [self.scrollView.tiledPDFView.layer setNeedsDisplay];
}

- (void)didReceiveMemoryWarning {
    NSLog(@"%s",__PRETTY_FUNCTION__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
