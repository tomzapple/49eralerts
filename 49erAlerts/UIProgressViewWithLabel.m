//
//  UIProgressViewWithLabel.m
//  49erAlerts
//
//  Created by lsta on 11/11/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import "UIProgressViewWithLabel.h"

@implementation UIProgressViewWithLabel

- (id)initWithLabel:(NSString*)labelString frame:(CGRect)frame{
    _labelString = labelString;
    return [self initWithFrame:frame];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self configureView];
    }
    return self;
}

-(void)configureView{
    
    self.backgroundColor = [UIColor clearColor];
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicator.frame = CGRectMake(0, 0, self.frame.size.height/2, self.frame.size.height/2);
    _activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _activityIndicator.backgroundColor = [UIColor clearColor];
    
    [self addSubview:_activityIndicator];
    
    CGFloat labelY = _activityIndicator.bounds.size.height + 2;
    
    _label = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, labelY, self.bounds.size.width , (self.frame.size.height/2) - 5.0f )];
    _label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    _label.font = [UIFont boldSystemFontOfSize:12.0f];
    _label.numberOfLines = 1;
    
    _label.backgroundColor = [UIColor clearColor];
    _label.textColor = [UIColor whiteColor];
    _label.text = _labelString;
    
    [self addSubview:_label];
}

-(void)startAnimating{
    [_activityIndicator startAnimating];
}

-(void)stopAnimating{
    [_activityIndicator stopAnimating];
    [_label removeFromSuperview];
}
@end
