//
//  ComputerAvailabilityViewController.m
//  49erAlerts
//
//  Created by lsta on 1/30/15.
//  Copyright (c) 2015 UNCCLibrary. All rights reserved.
//

#import "ComputerAvailabilityViewController.h"
#import "AppDelegate.h"

@interface ComputerAvailabilityViewController ()
@property NSDictionary *computerAvaliabilityData;

@end

@implementation ComputerAvailabilityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *URLString = [NSString stringWithFormat:@"http://library.uncc.edu/computers/json/"];
    NSURL *url = [NSURL URLWithString:URLString];
    
    [AppDelegate downloadDataFromURL:url withCompletionHandler:^(NSData *data) {
        // Check if any data returned.
        if (data != nil) {
            
            NSError *error;
            NSMutableDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if (error != nil) {
                NSLog(@"%@", [error localizedDescription]);
            }else{
                
                _computerAvaliabilityData = [returnedDict objectForKey:[_floorNumber stringValue]];
                
                _numberOfTerminals.text = [_computerAvaliabilityData[@"idle"] stringValue] ;
                _floorLabel.text = _floorNumber.stringValue;
                
                
            }
            
        }
    }];
    
    [_doneBarButton setTarget:self];
    [_doneBarButton setAction:@selector(aMethod:)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)aMethod:(id)sender{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    [appDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:^{
        ;
    }];
    
}
- (IBAction)viewFloorMap:(id)sender {
    NSURL *urlString = [NSURL URLWithString:[NSString stringWithFormat:@"http://library.uncc.edu/computers/?id=%@",_floorNumber]];
    [[UIApplication sharedApplication] openURL:urlString];
}

@end
