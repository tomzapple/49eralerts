//
//  ViewController.m
//  49erAlerts
//
//  Created by lsta on 10/2/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import "ViewController.h"
#import "OnboardingPageContentViewController.h"
#import "AppDelegate.h"

@interface ViewController ()
-(void)startOnboarding;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

//    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"hadGoneThroughOnboarding"]){
//        [self startOnboarding];
//        
//    }
//    else{
    
        //start the application
        // Create login view controller
//        self.loginNavigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"PDFRootViewController"];
//            [self.loginNavigationController didMoveToParentViewController:self];
//    }
    
    //no login for now
    //[self performSegueWithIdentifier:@"LocateBeacon" sender:self];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)startOnboarding {
    _pageTitles = @[@"Over 200 Tips and Tricks", @"Discover Hidden Features", @"Bookmark Favorite Tip", @"Free Regular Update"];
    _pageImages = @[@"page1.png", @"page2.png", @"page3.png", @"page4.png"];
    
    // Create page view controller
    self.onboardingPageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OnboardingPageViewController"];
    self.onboardingPageViewController.dataSource = self;
    
    OnboardingPageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.onboardingPageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.onboardingPageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    
    [self addChildViewController:_onboardingPageViewController];
    [self.view addSubview:_onboardingPageViewController.view];
    [self.onboardingPageViewController didMoveToParentViewController:self];
    
    
}
#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((OnboardingPageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((OnboardingPageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}


#pragma mark - Page View Controller Utility method
- ( OnboardingPageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    OnboardingPageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OnboardingPageContentViewController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}



#pragma mark - Page View Controller - PageIndicator methods

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}


#pragma mark - Page View Controller - Action methods
- (IBAction)startWalkThrough:(id)sender {
    
    if (self.pageImages.count < 1 || self.pageTitles.count < 1) {
        [self startOnboarding];
    }

    OnboardingPageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.onboardingPageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}

- (IBAction)startUsingApplication:(id)sender {
    NSLog(@"-------------Start Using 49er Alerts clicked--------------");
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hadGoneThroughOnboarding"];
    
    
}

- (IBAction)showTheMenuOptions:(id)sender {
    //[(AppDelegate *)[UIApplication sharedApplication] showLibraryOptionAlerts];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] showLibraryOptionAlerts];
    
    
}
@end
