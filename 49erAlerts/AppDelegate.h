//
//  AppDelegate.h
//  49erAlerts
//
//  Created by lsta on 10/2/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate, UIAlertViewDelegate >

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSData *pdfData;
- (void)showLibraryOptionAlerts;
+ (void)downloadDataFromURL:(NSURL *)url withCompletionHandler:(void (^)(NSData *))completionHandler;


@end

