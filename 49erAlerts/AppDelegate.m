//
//  AppDelegate.m
//  49erAlerts
//
//  Created by lsta on 10/2/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "UIProgressViewWithLabel.h"
#import "SimplePDFWebViewController.h"
#import "ComputerAvailabilityViewController.h"


NSString *const kGroupStudyRoomRegionIdentifier = @"kGroupStudyRoomRegionIdentifier";
NSString *const kBookDropRegionIdentifier = @"kBookDropRegionIdentifier";
NSString *const kInsideLibraryIdentifier = @"kInsideLibraryIdentifier";
NSString *const kFirstFloorIdentifier = @"kComputerAvailabilityIdentifier";

NSString *const kMainMenuClickedIdentifier = @"kMainMenuClickedIdentifier";

NSString *const kInsideLibraryCategory = @"kInsideLibraryCategory";
NSString *const kComputerAvailabilityCategory = @"kComputerAvailabilityCategory";

@interface AppDelegate ()
@property NSString *currentAlertIdentifier ;

@end


@implementation AppDelegate
{
    CLLocationManager *_locationManager;
}

@synthesize pdfData = _pdfData;

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    // A user can transition in or out of a region while the application is not running.
    // When this happens CoreLocation will launch the application momentarily, call this delegate method
    // and we will let the user know via a local notification.
    
    
    if(state == CLRegionStateInside)
    {
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        
        if ([region.identifier isEqualToString:kGroupStudyRoomRegionIdentifier] ) {
            notification.alertBody = @"You're near Group Study Room";
        }else if ([region.identifier isEqualToString:kBookDropRegionIdentifier]){
            notification.alertBody = @"You're near a Book Drop";
        }else if ([region.identifier isEqualToString:kFirstFloorIdentifier]){
            notification.alertBody = @"You are at the first floor. Study Rooms and Computers are available near you";
            notification.category = kComputerAvailabilityCategory;
        }else if ([region.identifier isEqualToString:kInsideLibraryIdentifier]){
            notification.alertBody = @"You have entered into the Atkins Library Region";
            notification.category = kInsideLibraryCategory;
        }
        NSLog(@"locationManager didDetermineState INSIDE for %@", region.identifier);
        
        
        _currentAlertIdentifier = region.identifier;
        
        // If the application is in the foreground, it will get a callback to application:didReceiveLocalNotification:.
        // If its not, iOS will display the notification to the user.
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
        
    }
    else if(state == CLRegionStateOutside)
    {
        //        if ([region.identifier isEqualToString:kGroupStudyRoomRegionIdentifier]) {
        //            notification.alertBody = @"You're outside Group Study Room region";
        //        }else if ([region.identifier isEqualToString:kBookDropRegionIdentifier]){
        //            notification.alertBody = @"You're outside a Book Drop";
        //        }else if ([region.identifier isEqualToString:kInsideLibraryIdentifier]){
        //            notification.alertBody = @"You're Outside the Atkins Library Region";
        //        }
        NSLog(@"locationManager didDetermineState OUTSIDE for %@", region.identifier);
    }
    else
    {
        NSLog(@"locationManager didDetermineState OTHER for %@", region.identifier);
        return;
    }
    
    
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIAlertView *alert ;
    // If the application is in the foreground, we will notify the user of the region's state via an alert.
    if([notification.category isEqualToString:kInsideLibraryCategory] ){
        alert = [[UIAlertView alloc] initWithTitle:notification.alertBody message:@"What would you like to know?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Explore Library Brochure",@"Upcoming Library Events",nil];
        
    }else if ([notification.category isEqualToString:kComputerAvailabilityCategory]){
        alert = [[UIAlertView alloc] initWithTitle:notification.alertBody message:@"What would you like to know?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View Computer Availability",@" View Group Study Room Availability",nil];
        
    }else{
        alert = [[UIAlertView alloc] initWithTitle:notification.alertBody message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    
    [alert show];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    UIMutableUserNotificationAction *acceptAction = [[UIMutableUserNotificationAction alloc] init];
    acceptAction.identifier = @"VIEW_IDENTIFIER";
    acceptAction.title = @"View";
    
    // Given seconds, not minutes, to run in the background
    acceptAction.activationMode = UIUserNotificationActivationModeForeground;
    
    // If YES the actions is red
    acceptAction.destructive = NO;
    
    // If YES requires passcode, but does not unlock the device
    acceptAction.authenticationRequired = YES;
    
    UIMutableUserNotificationCategory *viewCategory = [[UIMutableUserNotificationCategory alloc] init];
    viewCategory.identifier = @"VIEW_CATEGORY";
    
    // You can define up to 4 actions in the 'default' context
    // On the lock screen, only the first two will be shown
    // If you want to specify which two actions get used on the lockscreen, use UIUserNotificationActionContextMinimal
    [viewCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextDefault];
    // You can define up to 4 actions in the 'default' context
    // On the lock screen, only the first two will be shown
    // If you want to specify which two actions get used on the lockscreen, use UIUserNotificationActionContextMinimal
    [viewCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextDefault];
    
    NSSet *categories = [NSSet setWithObjects:viewCategory, nil];
    
    //register to get notified for alerts
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationActivationModeForeground categories:categories]];
    }
    
    // This location manager will be used to notify the user of region state transitions.
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager requestAlwaysAuthorization];
    _locationManager.delegate = self;
    
    
    
    //    CLBeaconRegion *region1;
    //
    //    region1 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"] major: 0 minor: 0 identifier: kBookDropRegionIdentifier];
    //    region1.notifyEntryStateOnDisplay = YES;
    //    region1.notifyOnEntry = YES;
    //    [_locationManager startMonitoringForRegion:region1];
    //[_locationManager stopRangingBeaconsInRegion:region1];
    // [_locationManager startRangingBeaconsInRegion:region1];kInsideLibraryIdentifier
    CLBeaconRegion *region2;
    
    region2 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"00000000-0000-0000-0000-000000000000"] major: 0 minor: 0 identifier: kInsideLibraryIdentifier];
    region2.notifyEntryStateOnDisplay = YES;
    region2.notifyOnEntry = YES;
    [_locationManager startMonitoringForRegion:region2];
    
    CLBeaconRegion *region3;
    
    region3 = [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:@"5AFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF"] major: 0 minor: 0 identifier: kFirstFloorIdentifier];
    region3.notifyEntryStateOnDisplay = YES;
    region3.notifyOnEntry = YES;
    
    [_locationManager startMonitoringForRegion:region3];
    
    
    
    //[_locationManager stopRangingBeaconsInRegion:region2];
    //[_locationManager startRangingBeaconsInRegion:region2];
    
    
    // Register our parse app with the service
    [Parse setApplicationId:@"6YjAYL0PDnvCiNh11TMYn0QFeR8Wk3ERHXx7j3rD"
                  clientKey:@"uAVcochzbxN27ll9qAYnNpnCcLBx8kRqK8YbI9Hr"];
    // Override point for customization after application launch.
    
    
    //Customizing the apps Onboarding screens page indicator control
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControl.backgroundColor = [UIColor whiteColor];
    
    if ( [[launchOptions valueForKey:UIApplicationLaunchOptionsLocationKey] boolValue]){
        NSLog(@"---- near a beacon launch----");
    }
    
    
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    // I commented out the line below because otherwise you see this every second in the logs
    NSLog(@"locationManager didRangeBeacons :Beacons: %@ Region:%@",beacons, region);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"---------application entering foreground---------------");
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
      if ([_currentAlertIdentifier isEqualToString:(kInsideLibraryIdentifier)] ) {
        if (buttonIndex == 1) {
            NSLog(@"Yes clicked");
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"BrochureNavigationController"];
                vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self.window.rootViewController presentViewController:vc animated:YES completion:NULL];
                
            });
            
            
        }else if (buttonIndex == 2){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"EventsNavigationController"];
                vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self.window.rootViewController presentViewController:vc animated:YES completion:NULL];
                
            });
            
        }
        
    }else if ([_currentAlertIdentifier isEqualToString:kFirstFloorIdentifier] ){
        if (buttonIndex == 1) {
            NSLog(@"Yes clicked");
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ComputerAvailabilityViewController *vc = (ComputerAvailabilityViewController*)[sb instantiateViewControllerWithIdentifier:@"ComputerAvailabilityViewController"];
                vc.floorNumber = [NSNumber numberWithInt:1];
                vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self.window.rootViewController presentViewController:vc animated:YES completion:NULL];
                
                //[progressViewWithLabel stopAnimating];
                //[indicator removeFromSuperview];
            });
        }
        if (buttonIndex == 2) {
            NSLog(@"Yes clicked");
            
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ComputerAvailabilityViewController *vc = (ComputerAvailabilityViewController*)[sb instantiateViewControllerWithIdentifier:@"GroupStudyAvailabilityViewController"];
                vc.floorNumber = [NSNumber numberWithInt:1];
                vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self.window.rootViewController presentViewController:vc animated:YES completion:NULL];
                
            });
        }
        
        
    }
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler {
    
    if ([identifier isEqualToString:@"VIEW_IDENTIFIER"]) {
        // handle it
        NSLog(@"Invite accepted! Handle that somehow...");
    }
    
    // Call this when you're finished
    completionHandler();
}

- (void) showLibraryOptionAlerts{
    _currentAlertIdentifier = kMainMenuClickedIdentifier;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Library Menu Options" message:@"What would you like to know about the Atkins Library?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Explore Library Brochure",@"Upcoming Library Events",@"View Computer Availability",@"View Group Study Room Availability",nil];
    [alert show];
}

+(void)downloadDataFromURL:(NSURL *)url withCompletionHandler:(void (^)(NSData *))completionHandler{
    // Instantiate a session configuration object.
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    // Instantiate a session object.
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    // Create a data task object to perform the data downloading.
    NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error != nil) {
            // If any error occurs then just display its description on the console.
            NSLog(@"%@", [error localizedDescription]);
        }
        else{
            // If no error occurs, check the HTTP status code.
            NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
            
            // If it's other than 200, then show it on the console.
            if (HTTPStatusCode != 200) {
                NSLog(@"HTTP status code = %d", HTTPStatusCode);
            }
            
            // Call the completion handler with the returned data on the main thread.
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                completionHandler(data);
            }];
        }
    }];
    
    // Resume the task.
    [task resume];
}



@end
