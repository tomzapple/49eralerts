//
//  SimplePDFWebViewController.h
//  49erAlerts
//
//  Created by lsta on 3/5/15.
//  Copyright (c) 2015 UNCCLibrary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimplePDFWebViewController : UIViewController
@property (nonatomic, strong) NSURL *pdfURL;
@end
