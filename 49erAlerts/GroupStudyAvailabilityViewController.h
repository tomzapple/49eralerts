//
//  GroupStudyAvailabilityViewController.h
//  49erAlerts
//
//  Created by lsta on 2/13/15.
//  Copyright (c) 2015 UNCCLibrary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupStudyAvailabilityViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *floorNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfGroupStudyRoomsAvailableLabel;
@property (strong) NSNumber* floorNumber;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBarButton;

@end
