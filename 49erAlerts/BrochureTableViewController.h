//
//  BrochureTableViewController.h
//  49erAlerts
//
//  Created by lsta on 3/5/15.
//  Copyright (c) 2015 UNCCLibrary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrochureTableViewController : UITableViewController
- (IBAction)goToMainScreen:(id)sender;

@end
