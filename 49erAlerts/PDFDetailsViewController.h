//
//  DetailsViewController.h
//  49erAlerts
//
//  Created by lsta on 10/17/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import "ViewController.h"
#import "PDFScrollView.h"


@interface PDFDetailsViewController : UIViewController
@property (strong) IBOutlet PDFScrollView* scrollView;

@property CGPDFDocumentRef pdf;
@property CGPDFPageRef page;
@property int pageNumber;
@property CGFloat myScale;
@end
