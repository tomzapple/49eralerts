//
//  MainMenuController.m
//  49erAlerts
//
//  Created by lsta on 4/3/15.
//  Copyright (c) 2015 UNCCLibrary. All rights reserved.
//

#import "MainMenuController.h"
#import "AppDelegate.h"
#import "ComputerAvailabilityViewController.h"
#import "GroupStudyAvailabilityViewController.h"

@interface MainMenuController ()
@property (nonatomic, strong) NSArray *mainMenuOptions;
@property (nonatomic, strong) AppDelegate *appDelegate;
@end
@implementation MainMenuController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    _mainMenuOptions = @[@"Explore Library Brochure",@"Upcoming Library Events",@"View Computer Availability",@"View Group Study Room Availability"];
    [self.tableView reloadData];

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [_mainMenuOptions count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell1";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    // Configure the cell with the textContent of the Post as the cell's text label
    //    PFObject *post = [_eventArray objectAtIndex:indexPath.row];
    NSString *menuOption = [_mainMenuOptions objectAtIndex:indexPath.row] ;
//    NSLog(@"Brochure File %@", [brochureNode objectForKey:@"fileName"]);
    [cell.textLabel setText:menuOption];
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Value Selected by user

    switch (indexPath.row) {
        {
        case 0:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
               UIViewController *viewController = [sb instantiateViewControllerWithIdentifier:@"BrochureNavigationController"];
                viewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self.navigationController presentViewController:viewController animated:YES completion:^{
                    NSLog(@"-------presented--------");
                }];
                
            });
            
            break;
        }
        {
        case 1:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *viewController = [sb instantiateViewControllerWithIdentifier:@"EventsNavigationController"];
                viewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self.navigationController presentViewController:viewController animated:YES completion:^{
                    NSLog(@"-------presented--------");
                }];
                
            });
            
            break;
        }
        {
            case 2:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                ComputerAvailabilityViewController *viewController = (ComputerAvailabilityViewController*)[sb instantiateViewControllerWithIdentifier:@"ComputerAvailabilityViewController"];
                viewController.floorNumber = [NSNumber numberWithInt:1];
                viewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self.navigationController presentViewController:viewController animated:YES completion:^{
                    NSLog(@"-------presented--------");
                }];
                
            });
            
            break;

        }
        {
        case 3:
            dispatch_async(dispatch_get_main_queue(), ^{
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                GroupStudyAvailabilityViewController *viewController = (GroupStudyAvailabilityViewController*)[sb instantiateViewControllerWithIdentifier:@"GroupStudyAvailabilityViewController"];
                                viewController.floorNumber = [NSNumber numberWithInt:1];
                viewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self.navigationController presentViewController:viewController animated:YES completion:^{
                    NSLog(@"-------presented--------");
                }];
                
            });
            
            break;
            
        }

        {
        default:
            break;
        }
    }



}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
//    
//    if ([[segue identifier] isEqualToString:@"ShowBrochureDetails"]) {
//        SimplePDFWebViewController *detailViewController = [segue destinationViewController];
//        
//        
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        
//        detailViewController.pdfURL = _brochureLinksArray[indexPath.row] ;
//        
//        
//    }
    
    
}


@end
