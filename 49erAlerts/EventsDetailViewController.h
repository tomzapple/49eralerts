//
//  EventsDetailViewController.h
//  49erAlerts
//
//  Created by lsta on 11/14/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *eventTitle;
@property (weak, nonatomic) IBOutlet UIImageView *eventImage;
@property (weak, nonatomic) IBOutlet UILabel *eventDetailsDescription;
@property (weak, nonatomic) IBOutlet UIButton *eventLink;
@property (nonatomic, strong) NSDictionary *event;

@end
