//
//  ComputerAvailabilityViewController.h
//  49erAlerts
//
//  Created by lsta on 1/30/15.
//  Copyright (c) 2015 UNCCLibrary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComputerAvailabilityViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *floorLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfTerminals;
@property (strong) NSNumber* floorNumber;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBarButton;

@end
