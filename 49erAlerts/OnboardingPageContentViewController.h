//
//  OnboardingPageContentViewController.h
//  49erAlerts
//
//  Created by lsta on 10/6/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnboardingPageContentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;

@end
