//
//  EventTableViewController.h
//  49erAlerts
//
//  Created by lsta on 11/13/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventTableViewController : UITableViewController
@property (nonatomic, strong) NSArray *eventArray;
- (IBAction)goToMainScreen:(id)sender;

@end
