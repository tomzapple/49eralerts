//
//  UIProgressViewWithLabel.h
//  49erAlerts
//
//  Created by lsta on 11/11/14.
//  Copyright (c) 2014 UNCCLibrary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIProgressViewWithLabel : UIView

@property (nonatomic) NSString* labelString;
@property (nonatomic) UILabel* label;
@property (nonatomic) UIActivityIndicatorView* activityIndicator;

- (id)initWithLabel:(NSString*)labelString frame:(CGRect)frame;
- (void)startAnimating;
- (void)stopAnimating;
@end
